FROM ubuntu:17.04

MAINTAINER Simon Sickle <simon@redeam.com>
LABEL version="1.0.0"
LABEL description="A general use Android docker container to build ROMs"

RUN dpkg --add-architecture i386
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y software-properties-common locales ca-certificates nano rsync sudo zip git build-essential wget libc6-i386
RUN apt-get install -y lib32stdc++6 lib32gcc1 lib32ncurses5 lib32z1 python curl psmisc module-init-tools python-pip unzip ssh
RUN apt-get install -y openssh-client autoconf automake intltool libtool curl make g++ gnupg flex bison gperf zlib1g-dev gcc-multilib
RUN apt-get install -y g++-multilib lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache libgl1-mesa-dev
RUN apt-get install -y libxml2-utils xsltproc unzip

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
# disable interactive functions
ENV DEBIAN_FRONTEND noninteractive

# Setup environment vars
ENV JAVA_VERSION 1.8
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle/

# Install java8
RUN add-apt-repository -y ppa:webupd8team/java && \
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
    apt-get update && \
    apt-get install -y --no-install-recommends oracle-java8-installer && \
    apt-get install -y --no-install-recommends oracle-java8-set-default && \
    rm -rf /var/cache/oracle-jdk8-installer

RUN java -version

RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && chmod a+x /usr/local/bin/repo

# GO to workspace
RUN mkdir -p /opt/workspace
WORKDIR /opt/workspace
